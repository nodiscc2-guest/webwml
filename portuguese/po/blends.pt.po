# Brazilian Portuguese translation for Debian website blends.pot
# Copyright (C) 2017 Software in the Public Interest, Inc.
#
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"PO-Revision-Date: 2017-02-28 21:34-0300\n"
"Last-Translator: Marcelo Gomes de Santana <marcelo@msantana.eng.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/blends/blend.defs:15
msgid "Metapackages"
msgstr "Metapacotes"

#: ../../english/blends/blend.defs:18
msgid "Downloads"
msgstr "Downloads"

#: ../../english/blends/blend.defs:21
msgid "Derivatives"
msgstr "Derivadas"

#: ../../english/blends/released.data:15
msgid ""
"The goal of Debian Astro is to develop a Debian based operating system that "
"fits the requirements of both professional and hobby astronomers. It "
"integrates a large number of software packages covering telescope control, "
"data reduction, presentation and other fields."
msgstr ""
"O objetivo da Debian Astro é desenvolver um sistema operacional baseado no "
"Debian que atenda às necessidades tanto de astrônomos profissionais quanto "
"de hobistas. Ela integra uma grande quantidade de pacotes de software "
"abrangendo controle de telescópio, redução de dados, apresentação e outras "
"áreas."

#: ../../english/blends/released.data:23
msgid ""
"The goal of DebiChem is to make Debian a good platform for chemists in their "
"day-to-day work."
msgstr ""
"O objetivo de DebiChem é fazer do Debian uma boa plataforma para químicos em "
"seus trabalhos diários"

#: ../../english/blends/released.data:31
#, fuzzy
#| msgid ""
#| "The goal of Debian Games is to provide games in Debian from arcade and "
#| "aventure to simulation and strategy."
msgid ""
"The goal of Debian Games is to provide games in Debian from arcade and "
"adventure to simulation and strategy."
msgstr ""
"O objetivo da Debian Games é disponibilizar jogos no Debian desde árcade e "
"aventura até simulação e estratégia."

#: ../../english/blends/released.data:39
msgid ""
"The goal of Debian Edu is to provide a Debian OS system suitable for "
"educational use and in schools."
msgstr ""
"O objetivo da Debian Edu é disponibilizar um sistema operacional Debian "
"adequado para uso educacional e em escolas."

#: ../../english/blends/released.data:47
msgid ""
"The goal of Debian GIS is to develop Debian into the best distribution for "
"Geographical Information System applications and users."
msgstr ""
"O objetivo da Debian GIS é transformar o Debian na melhor distribuição para "
"aplicações e usuários de Sistemas de Informações Geográficas."

#: ../../english/blends/released.data:57
msgid ""
"The goal of Debian Junior is to make Debian an OS that children will enjoy "
"using."
msgstr ""
"O objetivo da Debian Junior é fazer do Debian um sistema operacional que as "
"crianças gostem de usar."

#: ../../english/blends/released.data:65
msgid ""
"The goal of Debian Med is a complete free and open system for all tasks in "
"medical care and research. To achieve this goal Debian Med integrates "
"related free and open source software for medical imaging, bioinformatics, "
"clinic IT infrastructure, and others within the Debian OS."
msgstr ""
"O objetivo da Debian Med é um sistema aberto e completamente livre para "
"todas as tarefas de cuidados médicos e pesquisa. Para atingir esse objetivo, "
"a Debian Med integra softwares livres e de código aberto relacionados com "
"imagem médica, bioinformática, infraestrutura de TI clínica e outros no "
"sistema operacional Debian."

#: ../../english/blends/released.data:73
msgid ""
"The goal of Debian Multimedia is to make Debian a good platform for audio "
"and multimedia work."
msgstr ""
"O objetivo da Debian Multimedia é fazer do Debian uma boa plataforma para "
"trabalhos com áudio e multimídia."

#: ../../english/blends/released.data:81
msgid ""
"The goal of Debian Science is to provide a better experience when using "
"Debian to researchers and scientists."
msgstr ""
"O objetivo da Debian Science é proporcionar uma melhor experiência de uso do "
"Debian para pesquisadores e cientistas."

#: ../../english/blends/unreleased.data:15
msgid ""
"The goal of Debian Accessibility is to develop Debian into an operating "
"system that is particularly well suited for the requirements of people with "
"disabilities."
msgstr ""
"O objetivo da Debian Accessibility é transformar o Debian em um sistema "
"operacional que seja particularmente adequado às necessidades de pessoas com "
"deficiência."

#: ../../english/blends/unreleased.data:23
msgid ""
"The goal of Debian Design is to provide applications for designers. This "
"includes graphic design, web design and multimedia design."
msgstr ""
"O objetivo da Debian Design é disponibilizar aplicações para designers. Isso "
"inclui design gráfico, design web e design multimídia."

#: ../../english/blends/unreleased.data:30
msgid ""
"The goal of Debian EzGo is to provide culture-based open and free technology "
"with native language support and appropriate user friendly, lightweight and "
"fast desktop environment for low powerful/cost hardwares to empower human "
"capacity building and technology development  in many areas and regions, "
"like Africa, Afghanistan, Indonesia, Vietnam  using Debian."
msgstr ""
"O objetivo da Debian EzGo é disponibilizar tecnologia livre e aberta baseada "
"na cultura, com suporte ao idioma nativo e ambiente apropriado de área de "
"trabalho amigável ao usuário, leve e rápido, para hardwares de baixo custo e "
"pouco poder de processamento, para potencializar as capacidades humanas de "
"construção e desenvolvimento de tecnologia em muitas áreas e regiões, como "
"África, Afeganistão, Indonésia e Vietnã utilizando o Debian."

#: ../../english/blends/unreleased.data:38
msgid ""
"The goal of FreedomBox is to develop, design and promote personal servers "
"running free software for private, personal communications. Applications "
"include blogs, wikis, websites, social networks, email, web proxy and a Tor "
"relay on a device that can replace a wireless router so that data stays with "
"the users."
msgstr ""
"O objetivo da FreedomBox é desenvolver, projetar e promover servidores "
"pessoais que usem software livre para comunicações privadas e pessoais. As "
"aplicações incluem, blogs, sites web, redes sociais, e-mail, proxy web e um "
"retransmissor Tor no dispositivo, que pode substituir um roteador sem fio "
"para que os dados permaneçam com os usuários."

#: ../../english/blends/unreleased.data:46
msgid ""
"The goal of Debian Hamradio is to support the needs of radio amateurs in "
"Debian by providing logging, data mode and packet mode applications and more."
msgstr ""
"O objetivo da Debian Hamradio é dar suporte às necessidades de radioamadores "
"no Debian, disponibilizando aplicações de registro, modo de dados ou de "
"pacote, entre outras."

#: ../../english/blends/unreleased.data:55
msgid ""
"The goal of DebianParl is to provide applications to support the needs of "
"parliamentarians, politicians and their staffers all around the world."
msgstr ""
"O objetivo da DebianParl é disponibilizar aplicações que atendam às "
"necessidades dos parlamentares, políticos e seus funcionários ao redor do "
"mundo."

#~ msgid "Debian Accessibility"
#~ msgstr "Debian Accessibility"

#~ msgid "Debian Games"
#~ msgstr "Debian Games"
