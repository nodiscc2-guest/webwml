# Brazilian Portuguese translation for Debian website search.pot
# Copyright (C) 2003-2008 Software in the Public Interest, Inc.
#
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2008.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"PO-Revision-Date: 2008-05-02 21:22-0300\n"
"Last-Translator: Felipe Augusto van de Wiel (faw) <faw@debian.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/searchtmpl/search.def:6
msgid "Search for"
msgstr "Procurar por"

#: ../../english/searchtmpl/search.def:10
msgid "Results per page"
msgstr "Resultados por página"

#: ../../english/searchtmpl/search.def:14
msgid "Output format"
msgstr "Formato de saída"

#: ../../english/searchtmpl/search.def:18
msgid "Long"
msgstr "Longo"

#: ../../english/searchtmpl/search.def:22
msgid "Short"
msgstr "Curto"

#: ../../english/searchtmpl/search.def:26
msgid "URL"
msgstr "URL"

#: ../../english/searchtmpl/search.def:30
msgid "Default query type"
msgstr "Tipo de pesquisa padrão"

#: ../../english/searchtmpl/search.def:34
msgid "All Words"
msgstr "Todas as Palavras"

#: ../../english/searchtmpl/search.def:38
msgid "Any Words"
msgstr "Quaisquer Palavras"

#: ../../english/searchtmpl/search.def:42
msgid "Search through"
msgstr "Buscar através"

#: ../../english/searchtmpl/search.def:46
msgid "Entire site"
msgstr "Todo o site"

#: ../../english/searchtmpl/search.def:50
msgid "Language"
msgstr "Idioma"

#: ../../english/searchtmpl/search.def:54
msgid "Any"
msgstr "Qualquer"

#: ../../english/searchtmpl/search.def:58
msgid "Search results"
msgstr "Resultados de busca"

#: ../../english/searchtmpl/search.def:65
msgid ""
"Displaying documents \\$(first)-\\$(last) of total <B>\\$(total)</B> found."
msgstr ""
"Exibindo documentos \\$(first)-\\$(last) do total de <B>\\$(total)</B> "
"encontrados."

#: ../../english/searchtmpl/search.def:69
msgid ""
"Sorry, but search returned no results. <P>Some pages may be available in "
"English only, you may want to try searching again and set the Language to "
"Any."
msgstr ""
"Sinto muito, mas a busca não retornou resultados. <P>Algumas páginas podem "
"estar disponíveis apenas em inglês, você pode tentar procurar novamente "
"configurando a busca para qualquer idioma."

#: ../../english/searchtmpl/search.def:73
msgid "An error occured!"
msgstr "Ocorreu um erro!"

#: ../../english/searchtmpl/search.def:77
msgid "You should give at least one word to search for."
msgstr "Você deveria fornecer ao menos uma palavra para a busca."

#: ../../english/searchtmpl/search.def:81
msgid "Powered by"
msgstr "Powered by"

#: ../../english/searchtmpl/search.def:85
msgid "Next"
msgstr "Próxima"

#: ../../english/searchtmpl/search.def:89
msgid "Prev"
msgstr "Anterior"
