#!/bin/sh

# A simple script which checks that the included DLA data file matches the
# current DLA. dla-1000.wml should contain:
#include "$(ENGLISHDIR)/lts/security/2006/dla-1000.data"
#
# It also checks that the package versions in DLA match across
# all translations.
#
# It outputs all wrong files. There is no output if all files are OK.
# Japanese files are sometime false positives (because of a word search).
#
# Jens Seidel (jensseidel@users.sf.net), (c) 2006, GPL

if ! test -e english/security; then
  echo Please start $0 from the top level directory containing
  echo english/security
  echo
  exit 1
fi

# search for proper inclusion
for dir in $(find -name "security" -type d); do
  cd $dir
  for dla in $(find -name "dla-[0-9]*.wml" -o -name "[0-9]*.wml"); do
    file=$(echo $dla | sed 's,^.,$(ENGLISHDIR)/lts/security,;s,.wml$,.data,')
    grep --color=no "#include ['\"]$file['\"]" $dla > /dev/null || \
    (echo -n "$dir/$dla: "; grep "#include" $dla)
   done
   cd - > /dev/null
done

# search for proper version numbers
rm -f /tmp/dla
for dla in $(find english/security -name "dla-[0-9]*.wml"); do
  # append all lines to one to simplify search
  cat $dla | awk '{file=file " " $0} END {print file }' | sed 's/  * / /g' > .dla
  # extract the version string after $phrase
  for phrase in ' been fixed in version ' \
                ' will be fixed in version ' \
		' is fixed in version ' \
		' are fixed in version ' \
		' was fixed in version ' \
		' in version ' \
		' fixed in upstream version '
#		' fixed in the upstream version ' # "of Linux "
  do
    while grep "$phrase" .dla > /dev/null; do
      # matches the last version string (line limit of sed exceeds)
      #version=$(cat .dla | sed -n "s/^.*$phrase\([^,< ]*\).*$/\1/p" | sed 's/\.$//')
      version=$(cat .dla | perl -p -e "s/^.*$phrase([^,< ]*).*$/\1/" | sed 's/\.$//')
      version_regex=$(echo $version | sed 's/\./\\./g')
      # removed the last found match
      #cat .dla | sed "s/^\(.*\)$phrase\([^,< ]*\)\(.*\)$/\1\3/" > .dla.tmp; mv .dla.tmp .dla
      perl -p -i -e "s/^(.*)$phrase([^,< ]*)(.*)$/\1\3/" .dla
      for dla_tr in */$(echo $dla | sed 's,^english/,,'); do
        if ! grep -w "$version_regex" $dla_tr > /dev/null; then
          echo "Error: version $version does not occur in $dla_tr"
        fi
      done
    done
  done
  # record ignored version strings (to be analyzed later)
  cat .dla >> /tmp/dla
done

