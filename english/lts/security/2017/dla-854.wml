<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Icoutils is a set of programs that deal with MS Windows icons and
cursors. Resources such as icons and cursors can be extracted from
MS Windows executable and library files with wrestool.</p>

<p>Three vulnerabilities has been found in these tools.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6009">CVE-2017-6009</a>

    <p>A buffer overflow was observed in wrestool.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6010">CVE-2017-6010</a>

    <p>A buffer overflow was observed in the extract_icons function.
    This issue can be triggered by processing a corrupted ico file
    and will result in an icotool crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6011">CVE-2017-6011</a>

    <p>An out-of-bounds read leading to a buffer overflow was observed
    icotool.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.29.1-5deb7u2.</p>

<p>We recommend that you upgrade your icoutils packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

 <p>-------------- Ola Lundqvist /  opal@debian.org       GPG fingerprint          \
|  ola@inguza.com        22F2 32C6 B1E0 F4BF 2B26 |
|  <a href="http://inguza.com/">http://inguza.com/</a>    0A6A 5E90 DCFA 9426 876F /
 </p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-854.data"
# $Id: $
