<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple security issues have been found in Ming. They may lead
to the execution of arbitrary code or causing application crash.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9264">CVE-2016-9264</a>

    <p>global-buffer-overflow in printMP3Headers</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9265">CVE-2016-9265</a>

    <p>divide-by-zero in printMP3Headers</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9266">CVE-2016-9266</a>

    <p>left shift in listmp3.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9827">CVE-2016-9827</a>

    <p>listswf: heap-based buffer overflow in _iprintf</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9828">CVE-2016-9828</a>

    <p>listswf: heap-based buffer overflow in _iprintf</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9829">CVE-2016-9829</a>

    <p>listswf: NULL pointer dereference in dumpBuffer</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9831">CVE-2016-9831</a>

    <p>listswf: heap-based buffer overflow in parseSWF_RGBA</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.4.4-1.1+deb7u1.</p>

<p>We recommend that you upgrade your ming packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-799.data"
# $Id: $
