<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Request Tracker, an
extensible trouble-ticket tracking system. The Common Vulnerabilities
and Exposures project identifies the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6127">CVE-2016-6127</a>

    <p>It was discovered that Request Tracker is vulnerable to a cross-site
    scripting (XSS) attack if an attacker uploads a malicious file with
    a certain content type. Installations which use the
    AlwaysDownloadAttachments config setting are unaffected by this
    flaw. The applied fix addresses all existant and future uploaded
    attachments.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5361">CVE-2017-5361</a>

    <p>It was discovered that Request Tracker is vulnerable to timing
    side-channel attacks for user passwords.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5943">CVE-2017-5943</a>

    <p>It was discovered that Request Tracker is prone to an information
    leak of cross-site request forgery (CSRF) verification tokens if a
    user is tricked into visiting a specially crafted URL by an
    attacker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5944">CVE-2017-5944</a>

    <p>It was discovered that Request Tracker is prone to a remote code
    execution vulnerability in the dashboard subscription interface. A
    privileged attacker can take advantage of this flaw through
    carefully-crafted saved search names to cause unexpected code to be
    executed. The applied fix addresses all existant and future saved
    searches.</p>

<p>Additionally to the above mentioned CVEs, this update works around
<a href="https://security-tracker.debian.org/tracker/CVE-2015-7686">CVE-2015-7686</a> in Email::Address which could induce a denial of service
of Request Tracker itself.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.7-5+deb7u5.</p>

<p>We recommend that you upgrade your request-tracker4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>--u3/rZRmxL6MmkK24
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-987.data"
# $Id: $
