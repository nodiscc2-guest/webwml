<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were two vulnerabilities in libvncserver, a
library to create/embed a VNC server:</p>

<p>* <a href="https://security-tracker.debian.org/tracker/CVE-2016-9941">CVE-2016-9941</a>: Fix a heap-based buffer overflow that allows remote servers
  to cause a denial of service via a crafted FramebufferUpdate message
  containing a subrectangle outside of the drawing area.</p>

<p>* <a href="https://security-tracker.debian.org/tracker/CVE-2016-9942">CVE-2016-9942</a>: Fix a heap-based buffer overflow that allow remote servers
  to cause a denial of service via a crafted FramebufferUpdate message with
  the <q>Ultra</q> type tile such that the LZO decompressed payload exceeds the
  size of the tile dimensions.</p>

<p>For Debian 7 <q>Wheezy</q>, these issues have been fixed in libvncserver version
0.9.9+dfsg-1+deb7u2.</p>

<p>We recommend that you upgrade your libvncserver packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-777.data"
# $Id: $
