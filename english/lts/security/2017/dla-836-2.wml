<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The update for munin issued as DLA-836-1 caused a regression in the
zooming functionality in munin-cgi-graph. Updated packages are now
available to correct this issue. For reference, the original advisory
text follows.</p>

<p>Stevie Trujillo discovered a command injection vulnerability in munin,
a network-wide graphing framework. The CGI script for drawing graphs
allowed to pass arbitrary GET parameters to local shell command,
allowing command execution as the user that runs the webserver.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.0.6-4+deb7u4.</p>

<p>We recommend that you upgrade your munin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>


<p>Jonas Meurer</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-836-2.data"
# $Id: $
