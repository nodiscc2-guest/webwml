<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability and a data loss bug have been found in
postgresql-common, Debian's PostgreSQL database cluster management
tools.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1255">CVE-2016-1255</a>

    <p>Dawid Golunski discovered that a symlink in /var/log/postgresql/
    could be used by the <q>postgres</q> system user to write to arbitrary
    files on the filesystem the next time PostgreSQL is started by
    root.</p>

<p>#614374</p>

    <p>Rafał Kupka discovered that pg_upgradecluster did not properly
    upgrade databases that are owned by a non-login role (or group).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
134wheezy5.</p>

<p>We recommend that you upgrade your postgresql-common packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-774.data"
# $Id: $
