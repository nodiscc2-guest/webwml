<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were discovered in FreeRADIUS, a high-performance and
highly configurable RADIUS server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2015">CVE-2014-2015</a>

    <p>A stack-based buffer overflow was found in the normify function in
    the rlm_pap module, which can be attacked by existing users to
    cause denial of service or other issues.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-4680">CVE-2015-4680</a>

    <p>It was discovered that freeradius failed to check revocation of
    intermediate CA certificates, thus accepting client certificates
    issued by revoked certificates from intermediate CAs.</p>

    <p>Note that to enable checking of intermediate CA certificates, it
    is necessary to enable the check_all_crl option of the EAP TLS
    section in eap.conf. This is only necessary for servers using
    certificates signed by intermediate CAs. Servers that use
    self-signed CAs are unaffected.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9148">CVE-2017-9148</a>

    <p>The TLS session cache fails to reliably prevent resumption of an
    unauthenticated session, which allows remote attackers (such as
    malicious 802.1X supplicants) to bypass authentication via PEAP or
    TTLS.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.1.12+dfsg-1.2+deb7u1.</p>

<p>We recommend that you upgrade your freeradius packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-977.data"
# $Id: $
