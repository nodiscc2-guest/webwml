<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that ejabberd does not enforce the starttls_required
setting when compression is used, which causes clients to establish
connections without encryption.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
2.1.10-4+deb7u2.</p>

<p>This update also disables the insecure SSLv3.</p>

<p>We recommend that you upgrade your ejabberd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-881.data"
# $Id: $
