<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>tiff3 was affected by multiple memory leaks (<a href="https://security-tracker.debian.org/tracker/CVE-2017-9403">CVE-2017-9403</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-9404">CVE-2017-9404</a>)
that could result in denial of service. Furthermore, while the current
version in Debian was already patched for _TIFFVGetField issues
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-10095">CVE-2016-10095</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-9147">CVE-2017-9147</a>), we replaced our Debian-specific patches
by the upstream provided patches to stay closer to upstream.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.9.6-11+deb7u6.</p>

<p>We recommend that you upgrade your tiff3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-983.data"
# $Id: $
