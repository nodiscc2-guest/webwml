<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7960">CVE-2017-7960</a>

    <p>A heap-based buffer over-read vulnerability could be triggered
    remotely via a crafted CSS file to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7961">CVE-2017-7961</a>

    <p>An <q>outside the range of representable values of type long</q>
    undefined behavior issue was found in libcroco, which might
    allow remote attackers to cause a denial of service (application
    crash) or possibly have unspecified other impact via a crafted
    CSS file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.6.6-2+deb7u1.</p>

<p>We recommend that you upgrade your libcroco packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-909.data"
# $Id: $
