<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were two vulnerabilities in hesiod, Project
Athena's DNS-based directory service:</p>

  <p>* <a href="https://security-tracker.debian.org/tracker/CVE-2016-10151">CVE-2016-10151</a>: A weak SUID check allowing privilege elevation.</p>

  <p>* <a href="https://security-tracker.debian.org/tracker/CVE-2016-10152">CVE-2016-10152</a>: Use of a hard-coded DNS fallback domain
    (athena.mit.edu) if configuration file could not be read.</p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in hesiod version
3.0.2-21+deb7u1.</p>

<p>We recommend that you upgrade your hesiod packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-795.data"
# $Id: $
