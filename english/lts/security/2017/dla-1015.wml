<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was a key disclosure vulnerability in libgcrypt11
a library of cryptographic routines:</p>

  <p>It is well known that constant-time implementations of modular exponentiation
  cannot use sliding windows. However, software libraries such as Libgcrypt,
  used by GnuPG, continue to use sliding windows. It is widely believed that,
  even if the complete pattern of squarings and multiplications is observed
  through a side-channel attack, the number of exponent bits leaked is not
  sufficient to carry out a full key-recovery attack against RSA.
  Specifically, 4-bit sliding windows leak only 40% of the bits, and 5-bit
  sliding windows leak only 33% of the bits.</p>

    <p>-- Sliding right into disaster: Left-to-right sliding windows leak
       <https://eprint.iacr.org/2017/627></p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in libgcrypt11 version
1.5.0-5+deb7u6.</p>

<p>We recommend that you upgrade your libgcrypt11 packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1015.data"
# $Id: $
