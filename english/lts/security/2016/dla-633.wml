<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in wordpress, a web blogging
tool. The Common Vulnerabilities and Exposures project identifies the
following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8834">CVE-2015-8834</a>:

    <p>Cross-site scripting (XSS) vulnerability in wp-includes/wp-db.php in
    WordPress before 4.2.2 allows remote attackers to inject arbitrary
    web script or HTML via a long comment that is improperly stored
    because of limitations on the MySQL TEXT data type.
    NOTE: this vulnerability exists because of an incomplete fix for
    <a href="https://security-tracker.debian.org/tracker/CVE-2015-3440">CVE-2015-3440</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4029">CVE-2016-4029</a>:

    <p>WordPress before 4.5 does not consider octal and hexadecimal IP
    address formats when determining an intranet address, which allows
    remote attackers to bypass an intended SSRF protection mechanism
    via a crafted address.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5836">CVE-2016-5836</a>:

    <p>The oEmbed protocol implementation in WordPress before 4.5.3 allows
    remote attackers to cause a denial of service via unspecified
    vectors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6634">CVE-2016-6634</a>:

    <p>Cross-site scripting (XSS) vulnerability in the network settings
    page in WordPress before 4.5 allows remote attackers to inject
    arbitrary web script or HTML via unspecified vectors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6635">CVE-2016-6635</a>:

    <p>Cross-site request forgery (CSRF) vulnerability in the
    wp_ajax_wp_compression_test function in wp-admin/includes/ajax    actions.php in WordPress before 4.5 allows remote attackers to
    hijack the authentication of administrators for requests that
    change the script compression option.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7168">CVE-2016-7168</a>:

    <p>Fix a cross-site scripting vulnerability via image filename.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7169">CVE-2016-7169</a>:

    <p>Fix a path traversal vulnerability in the upgrade package uploader.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.1+dfsg-1~deb7u12.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-633.data"
# $Id: $
