<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in PostgreSQL, an SQL
database system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5423">CVE-2016-5423</a>

    <p>Karthikeyan Jambu Rajaraman discovered that nested CASE-WHEN
    expressions are not properly evaluated, potentially leading to a
    crash or allowing to disclose portions of server memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5424">CVE-2016-5424</a>

    <p>Nathan Bossart discovered that special characters in database and
    role names are not properly handled, potentially leading to the
    execution of commands with superuser privileges, when a superuser
    executes pg_dumpall or other routine maintenance operations.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
9.1.23-0+deb7u1.</p>

<p>We recommend that you upgrade your postgresql-9.1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-592.data"
# $Id: $
