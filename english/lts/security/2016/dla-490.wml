<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities have been discovered in bozohttpd, a small
HTTP server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-5015">CVE-2014-5015</a>

    <p>Bozotic HTTP server (aka bozohttpd) before 201407081 truncates
    paths when checking .htpasswd restrictions, which allows remote
    attackers to bypass the HTTP authentication scheme and access
    restrictions via a long path.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8212">CVE-2015-8212</a>

    <p>A flaw in CGI suffix handler support was found, if the -C option
    has been used to setup a CGI handler, that could result in remote
    code execution.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
20111118-1+deb7u1.</p>

<p>We recommend that you upgrade your bozohttpd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-490.data"
# $Id: $
