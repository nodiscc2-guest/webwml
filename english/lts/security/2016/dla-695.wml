<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities have been discovered in SPIP, a website
engine for publishing written in PHP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7980">CVE-2016-7980</a>

    <p>Nicolas Chatelain of Sysdream Labs discovered a cross-site request
    forgery (CSRF) vulnerability in the valider_xml action of SPIP. This
    allows remote attackers to make use of potential additional
    vulnerabilities such as the one described in <a href="https://security-tracker.debian.org/tracker/CVE-2016-7998">CVE-2016-7998</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7981">CVE-2016-7981</a>

    <p>Nicolas Chatelain of Sysdream Labs discovered a reflected cross-site
    scripting attack (XSS) vulnerability in the validater_xml action of
    SPIP. An attacker could take advantage of this vulnerability to
    inject arbitrary code by tricking an administrator to open a
    malicious link.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7982">CVE-2016-7982</a>

    <p>Nicolas Chatelain of Sysdream Labs discovered a file enumeration /
    path traversal attack in the the validator_xml action of SPIP. An
    attacker could use this to enumerate files in an arbitrary directory
    on the file system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7998">CVE-2016-7998</a>

    <p>Nicolas Chatelain of Sysdream Labs discovered a possible PHP code
    execution vulnerability in the template compiler/composer function
    of SPIP. In combination with the XSS and CSRF vulnerabilities
    described in this advisory, a remote attacker could take advantage
    of this to execute arbitrary PHP code on the server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7999">CVE-2016-7999</a>

    <p>Nicolas Chatelain of Sysdream Labs discovered a server side request
    forgery in the valider_xml action of SPIP. Attackers could take
    advantage of this vulnerability to send HTTP or FTP requests to
    remote servers that they don't have direct access to, possibly
    bypassing access controls such as a firewall.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.1.17-1+deb7u6.</p>

<p>We recommend that you upgrade your spip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>


<p>Jonas Meurer</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-695.data"
# $Id: $
