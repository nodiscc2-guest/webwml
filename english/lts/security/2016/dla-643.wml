<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities have been found in the CHICKEN Scheme compiler:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6830">CVE-2016-6830</a>

    <p>Buffer overrun in CHICKEN Scheme's "process-execute" and
"process-spawn" procedures from the posix unit</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6831">CVE-2016-6831</a>

    <p>Memory leak in CHICKEN Scheme's process-execute and process-spawn
procedures</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.7.0-1+deb7u1.</p>

<p>We recommend that you upgrade your chicken packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-643.data"
# $Id: $
