<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in libav:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1872">CVE-2015-1872</a>

    <p>The ff_mjpeg_decode_sof function in libavcodec/mjpegdec.c in Libav before
    0.8.18 does not validate the number of components in a JPEG-LS Start Of
    Frame segment, which allows remote attackers to cause a denial of service
    (out-of-bounds array access) or possibly have unspecified other impact via
    crafted Motion JPEG data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5479">CVE-2015-5479</a>

    <p>The ff_h263_decode_mba function in libavcodec/ituh263dec.c in Libav before
    11.5 allows remote attackers to cause a denial of service (divide-by-zero
    error and application crash) via a file with crafted dimensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7393">CVE-2016-7393</a>

    <p>The aac_sync function in libavcodec/aac_parser.c in Libav before 11.5 is
    vulnerable to a stack-based buffer overflow.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
6:0.8.18-0+deb7u1.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-644.data"
# $Id: $
