<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

    <p>* <a href="https://security-tracker.debian.org/tracker/CVE-2016-4473">CVE-2016-4473</a>.patch
      An invalid free may occur under certain conditions when processing
      phar-compatible archives.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-4538">CVE-2016-4538</a>.patch
      The bcpowmod function in ext/bcmath/bcmath.c in PHP before 5.5.35,
      5.6.x before 5.6.21, and 7.x before 7.0.6 accepts a negative integer
      for the scale argument, which allows remote attackers to cause a
      denial of service or possibly have unspecified other impact via a
      crafted call.
      (already fixed with patch for <a href="https://security-tracker.debian.org/tracker/CVE-2016-4537">CVE-2016-4537</a>)
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-5114">CVE-2016-5114</a>.patch
      sapi/fpm/fpm/fpm_log.c in PHP before 5.5.31, 5.6.x before 5.6.17,
      and 7.x before 7.0.2 misinterprets the semantics of the snprintf
      return value, which allows attackers to obtain sensitive information
      from process memory or cause a denial of service (out-of-bounds read
      and buffer overflow) via a long string, as demonstrated by a long URI
      in a configuration with custom REQUEST_URI logging.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-5399">CVE-2016-5399</a>.patch
      Improper error handling in bzread()
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-5768">CVE-2016-5768</a>.patch
      Double free vulnerability in the _php_mb_regex_ereg_replace_exec
      function in php_mbregex.c in the mbstring extension in PHP before
      5.5.37, 5.6.x before 5.6.23, and 7.x before 7.0.8 allows remote
      attackers to execute arbitrary code or cause a denial of service
      (application crash) by leveraging a callback exception.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-5769">CVE-2016-5769</a>.patch
      Multiple integer overflows in mcrypt.c in the mcrypt extension in
      PHP before 5.5.37, 5.6.x before 5.6.23, and 7.x before 7.0.8 allow
      remote attackers to cause a denial of service (heap-based buffer
      overflow and application crash) or possibly have unspecified other
      impact via a crafted length value, related to the
      (1) mcrypt_generic and (2) mdecrypt_generic functions.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-5770">CVE-2016-5770</a>.patch
      Integer overflow in the SplFileObject::fread function in
      spl_directory.c in the SPL extension in PHP before 5.5.37 and
      5.6.x before 5.6.23 allows remote attackers to cause a denial
      of service or possibly have unspecified other impact via a
      large integer argument, a related issue to <a href="https://security-tracker.debian.org/tracker/CVE-2016-5096">CVE-2016-5096</a>.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-5771">CVE-2016-5771</a>.patch
      spl_array.c in the SPL extension in PHP before 5.5.37 and 5.6.x
      before 5.6.23 improperly interacts with the unserialize
      implementation and garbage collection, which allows remote
      attackers to execute arbitrary code or cause a denial of service
      (use-after-free and application crash) via crafted serialized data.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-5772">CVE-2016-5772</a>.patch
      Double free vulnerability in the php_wddx_process_data function in
      wddx.c in the WDDX extension in PHP before 5.5.37, 5.6.x before
      5.6.23, and 7.x before 7.0.8 allows remote attackers to cause a
      denial of service (application crash) or possibly execute arbitrary
      code via crafted XML data that is mishandled in a wddx_deserialize
      call.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-5773">CVE-2016-5773</a>.patch
      php_zip.c in the zip extension in PHP before 5.5.37, 5.6.x before
      5.6.23, and 7.x before 7.0.8 improperly interacts with the
      unserialize implementation and garbage collection, which allows
      remote attackers to execute arbitrary code or cause a denial of
      service (use-after-free and application crash) via crafted
      serialized data containing a ZipArchive object.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-6289">CVE-2016-6289</a>.patch
      Integer overflow in the virtual_file_ex function in
      TSRM/tsrm_virtual_cwd.c in PHP before 5.5.38, 5.6.x before 5.6.24,
      and 7.x before 7.0.9 allows remote attackers to cause a denial of
      service (stack-based buffer overflow) or possibly have unspecified
      other impact via a crafted extract operation on a ZIP archive.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-6290">CVE-2016-6290</a>.patch
      ext/session/session.c in PHP before 5.5.38, 5.6.x before 5.6.24,
      and 7.x before 7.0.9 does not properly maintain a certain hash
      data structure, which allows remote attackers to cause a denial
      of service (use-after-free) or possibly have unspecified other
      impact via vectors related to session deserialization.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-6291">CVE-2016-6291</a>.patch
      The exif_process_IFD_in_MAKERNOTE function in ext/exif/exif.c in
      PHP before 5.5.38, 5.6.x before 5.6.24, and 7.x before 7.0.9 allows
      remote attackers to cause a denial of service (out-of-bounds array
      access and memory corruption), obtain sensitive information from
      process memory, or possibly have unspecified other impact via a
      crafted JPEG image.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-6292">CVE-2016-6292</a>.patch
      The exif_process_user_comment function in ext/exif/exif.c in PHP
      before 5.5.38, 5.6.x before 5.6.24, and 7.x before 7.0.9 allows
      remote attackers to cause a denial of service (NULL pointer
      dereference and application crash) via a crafted JPEG image.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-6294">CVE-2016-6294</a>.patch
      The locale_accept_from_http function in
      ext/intl/locale/locale_methods.c in PHP before 5.5.38, 5.6.x before
      5.6.24, and 7.x before 7.0.9 does not properly restrict calls to
      the ICU uloc_acceptLanguageFromHTTP function, which allows remote
      attackers to cause a denial of service (out-of-bounds read) or
      possibly have unspecified other impact via a call with a long argument.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-6295">CVE-2016-6295</a>.patch
      ext/snmp/snmp.c in PHP before 5.5.38, 5.6.x before 5.6.24, and 7.x
      before 7.0.9 improperly interacts with the unserialize implementation
      and garbage collection, which allows remote attackers to cause a
      denial of service (use-after-free and application crash) or possibly
      have unspecified other impact via crafted serialized data, a related
      issue to <a href="https://security-tracker.debian.org/tracker/CVE-2016-5773">CVE-2016-5773</a>.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-6296">CVE-2016-6296</a>.patch
      Integer signedness error in the simplestring_addn function in
      simplestring.c in xmlrpc-epi through 0.54.2, as used in PHP before
      5.5.38, 5.6.x before 5.6.24, and 7.x before 7.0.9, allows remote
      attackers to cause a denial of service (heap-based buffer overflow)
      or possibly have unspecified other impact via a long first argument
      to the PHP xmlrpc_encode_request function.
    * <a href="https://security-tracker.debian.org/tracker/CVE-2016-6297">CVE-2016-6297</a>.patch
      Integer overflow in the php_stream_zip_opener function in
      ext/zip/zip_stream.c in PHP before 5.5.38, 5.6.x before 5.6.24, and
      7.x before 7.0.9 allows remote attackers to cause a denial of
      service (stack-based buffer overflow) or possibly have unspecified
      other impact via a crafted zip:// URL.
    * BUG-70436.patch
      Use After Free Vulnerability in unserialize()
    * BUG-72681.patch
      PHP Session Data Injection Vulnerability, consume data even if we're
      not storing them.</p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in php5 version 5.4.45-0+deb7u5</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-628.data"
# $Id: $
