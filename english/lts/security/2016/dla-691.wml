<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4658">CVE-2016-4658</a>

      <p>Namespace nodes must be copied to avoid use-after-free errors.
      But they don't necessarily have a physical representation in a
      document, so simply disallow them in XPointer ranges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5131">CVE-2016-5131</a>

      <p>The old code would invoke the broken xmlXPtrRangeToFunction.
      range-to isn't really a function but a special kind of
      location step. Remove this function and always handle range-to
      in the XPath code.
      The old xmlXPtrRangeToFunction could also be abused to trigger
      a use-after-free error with the potential for remote code
      execution.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.8.0+dfsg1-7+wheezy7.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-691.data"
# $Id: $
