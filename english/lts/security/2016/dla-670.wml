<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8956">CVE-2015-8956</a>

    <p>It was discovered that missing input sanitising in RFCOMM Bluetooth
    socket handling may result in denial of service or information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5195">CVE-2016-5195</a>

    
    <p>It was discovered that a race condition in the memory management
    code can be used for local privilege escalation.  This does not
    affect kernels built with PREEMPT_RT enabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7042">CVE-2016-7042</a>

    
    <p>Ondrej Kozina discovered that incorrect buffer allocation in the
    proc_keys_show() function may result in local denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7425">CVE-2016-7425</a>

    <p>Marco Grassi discovered a buffer overflow in the arcmsr SCSI driver
    which may result in local denial of service, or potentially,
    arbitrary code execution.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.82-1.  This version also includes bug fixes from upstream version
3.2.82 and updates the PREEMPT_RT featureset to version 3.2.82-rt119.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.16.36-1+deb8u2.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-670.data"
# $Id: $
