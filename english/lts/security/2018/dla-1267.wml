<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Squid, a high-performance proxy caching server for web clients, has been
found vulnerable to denial of service attacks associated with ESI
response processing and intermediate CA certificate downloading.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000027">CVE-2018-1000027</a>

    <p>Incorrect pointer handling resulted in the possibility of a remote
    client delivering certain HTTP requests in conjunction with certain
    trusted server reponses involving the processing of ESI responses or
    downloading of intermediate CA certificates to trigger a denial of
    service for all clients accessing the squid service.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.7.STABLE9-4.1+deb7u3.</p>

<p>We recommend that you upgrade your squid packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1267.data"
# $Id: $
