<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A regression issue has been resolved in the poppler PDF rendering
shared library introduced with version 0.26.5-2+deb8u5.</p>

<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16646">CVE-2018-16646</a>

    <p>In Poppler 0.68.0, the Parser::getObj() function in Parser.cc may
    cause infinite recursion via a crafted file. A remote attacker can
    leverage this for a DoS attack.</p>

    <p>The previous solution in Debian LTS fixed the above issue in XRef.cc,
    the patches had been obtained from a merge request (#67) on upstream's
    Git development platform. Unfortunately, this merge request was declined
    by upstream and another merge request (#91) got applied instead. The
    fix now directly occurs in the Parser.cc file.</p>

    <p>This version of poppler now ships the changeset that got favorized by
    the poppler upstream developers (MR #91) and drops the patches from
    MR #67.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.26.5-2+deb8u6.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1562-2.data"
# $Id: $
