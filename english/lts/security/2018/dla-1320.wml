<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in Samba, a SMB/CIFS file,
print, and login server for Unix. The Common Vulnerabilities and
Exposures project identifies the following issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1050">CVE-2018-1050</a>

    <p>It was discovered that Samba is prone to a denial of service
    attack when the RPC spoolss service is configured to be run as an
    external daemon. Thanks for Jeremy Allison for the patch.</p>

    <p>https://www.samba.org/samba/security/<a href="https://security-tracker.debian.org/tracker/CVE-2018-1050">CVE-2018-1050</a>.html</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.6-6+deb7u16.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1320.data"
# $Id: $
