<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Ming:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6358">CVE-2018-6358</a>

    <p>Heap-based buffer overflow vulnerability in the printDefineFont2 function
    (util/listfdb.c). Remote attackers might leverage this vulnerability to
    cause a denial of service via a crafted swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7867">CVE-2018-7867</a>

    <p>Heap-based buffer overflow vulnerability in the getString function
    (util/decompile.c) during a RegisterNumber sprintf. Remote attackers might
    leverage this vulnerability to cause a denial of service via a crafted swf
    file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7868">CVE-2018-7868</a>

    <p>Heap-based buffer over-read vulnerability in the getName function
    (util/decompile.c) for CONSTANT8 data. Remote attackers might leverage this
    vulnerability to cause a denial of service via a crafted swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7870">CVE-2018-7870</a>

    <p>Invalid memory address dereference in the getString function
    (util/decompile.c) for CONSTANT16 data. Remote attackers might leverage this
    vulnerability to cause a denial of service via a crafted swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7871">CVE-2018-7871</a>

    <p>Heap-based buffer over-read vulnerability in the getName function
    (util/decompile.c) for CONSTANT16 data. Remote attackers might leverage this
    vulnerability to cause a denial of service via a crafted swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7872">CVE-2018-7872</a>

    <p>Invalid memory address dereference in the getName function
    (util/decompile.c) for CONSTANT16 data. Remote attackers might leverage this
    vulnerability to cause a denial of service via a crafted swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7875">CVE-2018-7875</a>

    <p>Heap-based buffer over-read vulnerability in the getName function
    (util/decompile.c) for CONSTANT8 data. Remote attackers might leverage this
    vulnerability to cause a denial of service via a crafted swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9165">CVE-2018-9165</a>

    <p>The pushdup function (util/decompile.c) performs shallow copy of String
    elements (instead of deep copy), allowing simultaneous change of multiple
    elements of the stack, which indirectly makes the library vulnerable to a
    NULL pointer dereference in getName (util/decompile.c). Remote attackers
    might leverage this vulnerability to cause dos via a crafted swf file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.4.4-1.1+deb7u8.</p>

<p>We recommend that you upgrade your ming packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1343.data"
# $Id: $
