<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The ZipCommon::isValidPath() function in Zip/src/ZipCommon.cpp in POCO 
C++ Libraries before 1.8 does not properly restrict the filename value 
in the ZIP header, which allows attackers to conduct absolute path 
traversal attacks during the ZIP decompression, and possibly create or 
overwrite arbitrary files, via a crafted ZIP file, related to a <q>file 
path injection vulnerability</q>.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.6p1-4+deb7u1.</p>

<p>We recommend that you upgrade your poco packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>--tThc/1wpZn/ma/RB
Content-Type: application/pgp-signature; name="signature.asc"</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1239.data"
# $Id: $
