<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in tinc, a Virtual Private
Network (VPN) daemon. The Common Vulnerabilities and Exposures project
identifies the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16737">CVE-2018-16737</a>

    <p>Michael Yonli discovered a flaw in the implementation of the
    authentication protocol that could allow a remote attacker to
    establish an authenticated, one-way connection with another node.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16758">CVE-2018-16758</a>

    <p>Michael Yonli discovered that a man-in-the-middle that has
    intercepted a TCP connection might be able to disable encryption of
    UDP packets sent by a node.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.0.24-2+deb8u1.</p>

<p>We recommend that you upgrade your tinc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1538.data"
# $Id: $
