<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>ClamAV is an anti-virus utility for Unix, whose upstream developers have
released the version 0.100.2. Installing this new version is required to
make use of all current virus signatures and to avoid warnings.</p>

<p>This version also fixes a security issue discovered after version 0.100.1:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15378">CVE-2018-15378</a>:

    <p>A vulnerability in ClamAV's MEW unpacker may allow unauthenticated
    remote offenders to cause a denial of service (DoS) via a specially
    crafted EXE file.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.100.2+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>--cmJC7u66zC7hs+87
Content-Type: application/pgp-signature; name="signature.asc"</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1553.data"
# $Id: $
