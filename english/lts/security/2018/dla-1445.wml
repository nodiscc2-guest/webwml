<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Busybox, utility programs for small and embedded systems, was affected
by several security vulnerabilities. The Common Vulnerabilities and
Exposures project identifies the following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2011-5325">CVE-2011-5325</a>

    <p>A path traversal vulnerability was found in Busybox implementation
    of tar. tar will extract a symlink that points outside of the
    current working directory and then follow that symlink when
    extracting other files. This allows for a directory traversal
    attack when extracting untrusted tarballs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-1813">CVE-2013-1813</a>

    <p>When device node or symlink in /dev should be created inside
    2-or-deeper subdirectory (/dev/dir1/dir2.../node), the intermediate
    directories are created with incorrect permissions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4607">CVE-2014-4607</a>

    <p>An integer overflow may occur when processing any variant of a
   <q>literal run</q> in the lzo1x_decompress_safe function. Each of these
    three locations is subject to an integer overflow when processing
    zero bytes. This exposes the code that copies literals to memory
    corruption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9645">CVE-2014-9645</a>

    <p>The add_probe function in modutils/modprobe.c in BusyBox allows
    local users to bypass intended restrictions on loading kernel
    modules via a / (slash) character in a module name, as demonstrated
    by an "ifconfig /usbserial up" command or a "mount -t /snd_pcm none
    /" command.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2147">CVE-2016-2147</a>

    <p>Integer overflow in the DHCP client (udhcpc) in BusyBox allows
    remote attackers to cause a denial of service (crash) via a
    malformed RFC1035-encoded domain name, which triggers an
    out-of-bounds heap write.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2148">CVE-2016-2148</a>

    <p>Heap-based buffer overflow in the DHCP client (udhcpc) in BusyBox
    allows remote attackers to have unspecified impact via vectors
    involving OPTION_6RD parsing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15873">CVE-2017-15873</a>

    <p>The get_next_block function in archival/libarchive
    /decompress_bunzip2.c in BusyBox has an Integer Overflow that may
    lead to a write access violation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16544">CVE-2017-16544</a>

    <p>In the add_match function in libbb/lineedit.c in BusyBox, the tab
    autocomplete feature of the shell, used to get a list of filenames
    in a directory, does not sanitize filenames and results in executing
    any escape sequence in the terminal. This could potentially result
    in code execution, arbitrary file writes, or other attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000517">CVE-2018-1000517</a>

    <p>BusyBox contains a Buffer Overflow vulnerability in
    Busybox wget that can result in a heap-based buffer overflow.
    This attack appears to be exploitable via network connectivity.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-9621">CVE-2015-9621</a>

    <p>Unziping a specially crafted zip file results in a computation of an
    invalid pointer and a crash reading an invalid address.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:1.22.0-9+deb8u2.</p>

<p>We recommend that you upgrade your busybox packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1445.data"
# $Id: $
