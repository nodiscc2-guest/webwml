<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Ming:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5251">CVE-2018-5251</a>

    <p>Integer signedness error vulnerability (left shift of a negative value) in
    the readSBits function (util/read.c). Remote attackers can leverage this
    vulnerability to cause a denial of service via a crafted swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5294">CVE-2018-5294</a>

    <p>Integer overflow vulnerability (caused by an out-of-range left shift) in
    the readUInt32 function (util/read.c). Remote attackers could leverage this
    vulnerability to cause a denial-of-service via a crafted swf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6315">CVE-2018-6315</a>

    <p>Integer overflow and resultant out-of-bounds read in the
    outputSWF_TEXT_RECORD function (util/outputscript.c). Remote attackers
    could leverage this vulnerability to cause a denial of service or
    unspecified other impact via a crafted SWF file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6359">CVE-2018-6359</a>

    <p>Use-after-free vulnerability in the decompileIF function
    (util/decompile.c). Remote attackers could leverage this vulnerability to
    cause a denial of service or unspecified other impact via a crafted SWF
    file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.4.4-1.1+deb7u7.</p>

<p>We recommend that you upgrade your ming packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1305.data"
# $Id: $
