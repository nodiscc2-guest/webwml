<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in qemu, a fast processor emulator:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8666">CVE-2015-8666</a>

    <p>Heap-based buffer overflow in QEMU when built with the
    Q35-chipset-based PC system emulator</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2198">CVE-2016-2198</a>

    <p>Null pointer dereference in ehci_caps_write in the USB EHCI support
    that may result in denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6833">CVE-2016-6833</a>

    <p>Use after free while writing in the vmxnet3 device that could be used
    to cause a denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6835">CVE-2016-6835</a>

    <p>Buffer overflow in vmxnet_tx_pkt_parse_headers() in vmxnet3 device
    that could result in denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8576">CVE-2016-8576</a>

    <p>Infinite loop vulnerability in xhci_ring_fetch in the USB xHCI support</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8667">CVE-2016-8667</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2016-8669">CVE-2016-8669</a></p>

    <p>Divide by zero errors in set_next_tick in the JAZZ RC4030 chipset
    emulator, and in serial_update_parameters of some serial devices, that
    could result in denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9602">CVE-2016-9602</a>

   <p>Improper link following with VirtFS</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9603">CVE-2016-9603</a>

    <p>Heap buffer overflow via vnc connection in the Cirrus CLGD 54xx VGA
    emulator support</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9776">CVE-2016-9776</a>

    <p>Infinite loop while receiving data in the ColdFire Fast Ethernet
    Controller emulator</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9907">CVE-2016-9907</a>

    <p>Memory leakage in the USB redirector usb-guest support </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9911">CVE-2016-9911</a>

    <p>Memory leakage in ehci_init_transfer in the USB EHCI support</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9914">CVE-2016-9914</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2016-9915">CVE-2016-9915</a> / <a href="https://security-tracker.debian.org/tracker/CVE-2016-9916">CVE-2016-9916</a></p>

    <p>Plan 9 File System (9pfs): add missing cleanup operation in
    FileOperations, in the handle backend and in the proxy backend driver</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9921">CVE-2016-9921</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2016-9922">CVE-2016-9922</a></p>

    <p>Divide by zero in cirrus_do_copy in the Cirrus CLGD 54xx VGA Emulator
    support </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10155">CVE-2016-10155</a>

    <p>Memory leak in hw/watchdog/wdt_i6300esb.c allowing local guest OS
    privileged users to cause a denial of service via a large number of
    device unplug operations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2615">CVE-2017-2615</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2017-2620">CVE-2017-2620</a> / <a href="https://security-tracker.debian.org/tracker/CVE-2017-18030">CVE-2017-18030</a> / <a href="https://security-tracker.debian.org/tracker/CVE-2018-5683">CVE-2018-5683</a> / <a href="https://security-tracker.debian.org/tracker/CVE-2017-7718">CVE-2017-7718</a></p>

    <p>Out-of-bounds access issues in the Cirrus CLGD 54xx VGA emulator
    support, that could result in denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5525">CVE-2017-5525</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2017-5526">CVE-2017-5526</a></p>

    <p>Memory leakage issues in the ac97 and es1370 device emulation</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5579">CVE-2017-5579</a>

    <p>Most memory leakage in the 16550A UART emulation</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5667">CVE-2017-5667</a>

    <p>Out-of-bounds access during multi block SDMA transfer in the SDHCI
    emulation support.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">CVE-2017-5715</a>

    <p>Mitigations against the Spectre v2 vulnerability. For more information
    please refer to <a href="https://www.qemu.org/2018/01/04/spectre/">https://www.qemu.org/2018/01/04/spectre/</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5856">CVE-2017-5856</a>

    <p>Memory leak in the MegaRAID SAS 8708EM2 Host Bus Adapter emulation
    support</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5973">CVE-2017-5973</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2017-5987">CVE-2017-5987</a> / <a href="https://security-tracker.debian.org/tracker/CVE-2017-6505">CVE-2017-6505</a></p>

    <p>Infinite loop issues in the USB xHCI, in the transfer mode register
    of the SDHCI protocol, and the USB ohci_service_ed_list</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7377">CVE-2017-7377</a>

    <p>9pfs: host memory leakage via v9fs_create</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7493">CVE-2017-7493</a>

    <p>Improper access control issues in the host directory sharing via
    9pfs support.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7980">CVE-2017-7980</a>

    <p>Heap-based buffer overflow in the Cirrus VGA device that could allow
    local guest OS users to execute arbitrary code or cause a denial of
    service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8086">CVE-2017-8086</a>

    <p>9pfs: host memory leakage via v9pfs_list_xattr</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8112">CVE-2017-8112</a>

    <p>Infinite loop in the VMWare PVSCSI emulation</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8309">CVE-2017-8309</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2017-8379">CVE-2017-8379</a></p>

    <p>Host memory leakage issues via the audio capture buffer and the
    keyboard input event handlers </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9330">CVE-2017-9330</a>

    <p>Infinite loop due to incorrect return value in USB OHCI that may
    result in denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9373">CVE-2017-9373</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2017-9374">CVE-2017-9374</a></p>

    <p>Host memory leakage during hot unplug in IDE AHCI and USB emulated
    devices that could result in denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9503">CVE-2017-9503</a>

    <p>Null pointer dereference while processing megasas command</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10806">CVE-2017-10806</a>

    <p>Stack buffer overflow in USB redirector</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10911">CVE-2017-10911</a>

    <p>Xen disk may leak stack data via response ring</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11434">CVE-2017-11434</a>

    <p>Out-of-bounds read while parsing Slirp/DHCP options</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14167">CVE-2017-14167</a>

    <p>Out-of-bounds access while processing multiboot headers that could
    result in the execution of arbitrary code</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15038">CVE-2017-15038</a>

    <p>9pfs: information disclosure when reading extended attributes</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15289">CVE-2017-15289</a>

    <p>Out-of-bounds write access issue in the Cirrus graphic adaptor that
    could result in denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16845">CVE-2017-16845</a>

    <p>Information leak in the PS/2 mouse and keyboard emulation support that
    could be exploited during instance migration </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18043">CVE-2017-18043</a>

    <p>Integer overflow in the macro ROUND_UP (n, d) that could result in
    denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7550">CVE-2018-7550</a>

    <p>Incorrect handling of memory during multiboot that could may result in
    execution of arbitrary code</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.1+dfsg-12+deb8u7.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>--+HP7ph2BbKc20aGI
Content-Type: application/pgp-signature; name="signature.asc"</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1497.data"
# $Id: $
