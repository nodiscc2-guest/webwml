<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Ruby 2.1.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2337">CVE-2016-2337</a>

    <p>Type confusion exists in _cancel_eval Ruby's TclTkIp class
    method. Attacker passing different type of object than String as
    <q>retval</q> argument can cause arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000073">CVE-2018-1000073</a>

    <p>RubyGems contains a Directory Traversal vulnerability in
    install_location function of package.rb that can result in path
    traversal when writing to a symlinked basedir outside of the root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000074">CVE-2018-1000074</a>

    <p>RubyGems contains a Deserialization of Untrusted Data
    vulnerability in owner command that can result in code
    execution. This attack appear to be exploitable via victim must
    run the `gem owner` command on a gem with a specially crafted YAML
    file.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.5-2+deb8u5.</p>

<p>We recommend that you upgrade your ruby2.1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1480.data"
# $Id: $
