<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were found in Bouncy
Castle, a Java implementation of cryptographic algorithms.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000338">CVE-2016-1000338</a>

    <p>DSA does not fully validate ASN.1 encoding of signature on
    verification. It is possible to inject extra elements in the
    sequence making up the signature and still have it validate, which
    in some cases may allow the introduction of <q>invisible</q> data into a
    signed structure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000339">CVE-2016-1000339</a>

    <p>Previously the primary engine class used for AES was AESFastEngine.
    Due to the highly table driven approach used in the algorithm it
    turns out that if the data channel on the CPU can be monitored the
    lookup table accesses are sufficient to leak information on the AES
    key being used. There was also a leak in AESEngine although it was
    substantially less. AESEngine has been modified to remove any signs
    of leakage and is now the primary AES class for the BC JCE provider.
    Use of AESFastEngine is now only recommended where otherwise deemed
    appropriate.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000341">CVE-2016-1000341</a>

    <p>DSA signature generation is vulnerable to timing attack. Where
    timings can be closely observed for the generation of signatures,
    the lack of blinding may allow an attacker to gain information about
    the signature's k value and ultimately the private value as well.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000342">CVE-2016-1000342</a>

    <p>ECDSA does not fully validate ASN.1 encoding of signature on
    verification. It is possible to inject extra elements in the
    sequence making up the signature and still have it validate, which
    in some cases may allow the introduction of <q>invisible</q> data into a
    signed structure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000343">CVE-2016-1000343</a>

    <p>The DSA key pair generator generates a weak private key if used with
    default values. If the JCA key pair generator is not explicitly
    initialised with DSA parameters, 1.55 and earlier generates a
    private value assuming a 1024 bit key size. In earlier releases this
    can be dealt with by explicitly passing parameters to the key pair
    generator.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000345">CVE-2016-1000345</a>

    <p>The DHIES/ECIES CBC mode is vulnerable to padding oracle attack. In
    an environment where timings can be easily observed, it is possible
    with enough observations to identify when the decryption is failing
    due to padding.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000346">CVE-2016-1000346</a>

    <p>In the Bouncy Castle JCE Provider the other party DH public key is
    not fully validated. This can cause issues as invalid keys can be
    used to reveal details about the other party's private key where
    static Diffie-Hellman is in use. As of this release the key
    parameters are checked on agreement calculation.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.49+dfsg-3+deb8u3.</p>

<p>We recommend that you upgrade your bouncycastle packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1418.data"
# $Id: $
