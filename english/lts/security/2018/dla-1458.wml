<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the Apache XML Security for C++ library performed
insufficient validation of KeyInfo hints, which could result in denial
of service via NULL pointer dereferences when processing malformed XML
data.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.7.2-3+deb8u1.</p>

<p>We recommend that you upgrade your xml-security-c packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>--=-=-Content-Type: application/pgp-signature; name="signature.asc"</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1458.data"
# $Id: $
