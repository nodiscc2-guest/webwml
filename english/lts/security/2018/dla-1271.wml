<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerabilities has been found in the PostgreSQL database system:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1053">CVE-2018-1053</a>

    <p>Tom Lane discovered that pg_upgrade, a tool used to upgrade
    PostgreSQL database clusters, creates temporary files containing
    password hashes that are world-readable.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
9.1.24lts2-0+deb7u2.</p>

<p>We recommend that you upgrade your postgresql-9.1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1271.data"
# $Id: $
