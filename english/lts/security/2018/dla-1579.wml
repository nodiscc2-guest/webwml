<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in openjpeg2, the
open-source JPEG 2000 codec.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17480">CVE-2017-17480</a>

  <p>Write stack buffer overflow due to missing buffer length formatter in
  fscanf call (jp3d and jpwl codecs). This vulnerability might be
  leveraged by remote attackers using crafted jp3d and jpwl files to
  cause denial of service or possibly remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18088">CVE-2018-18088</a>

  <p>Null pointer dereference caused by null image components in imagetopnm.
  This vulnerability might be leveraged by remote attackers using crafted
  BMP files to cause denial of service.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.0-2+deb8u5.</p>

<p>We recommend that you upgrade your openjpeg2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1579.data"
# $Id: $
