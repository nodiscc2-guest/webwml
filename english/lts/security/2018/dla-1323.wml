<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2018d. Notable
changes are:
 - Palestine started Daylight Saving Time (DST) on March 24,
   rather than on March 31st.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2018d-0+deb7u1.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1323.data"
# $Id: $
