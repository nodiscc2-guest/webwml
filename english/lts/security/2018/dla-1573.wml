<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
                 <p><a href="https://security-tracker.debian.org/tracker/CVE-2017-13078">CVE-2017-13078</a> <a href="https://security-tracker.debian.org/tracker/CVE-2017-13079">CVE-2017-13079</a> <a href="https://security-tracker.debian.org/tracker/CVE-2017-13080">CVE-2017-13080</a> <a href="https://security-tracker.debian.org/tracker/CVE-2017-13081">CVE-2017-13081</a>
Debian Bug     : 620066 724970 769633 774914 790061 793544 793874 795303
                 800090 800440 800820 801514 802970 803920 808792 816350
		 823402 823637 826996 832925 833355 833876 838038 838476
		 838858 841092 842762 854695 854907 856853 862458 869639
		 907320</p>

<p>Several vulnerabilities have been discovered in the firmware for
Broadcom BCM43xx wifi chips that may lead to a privilege escalation
or loss of confidentiality.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0801">CVE-2016-0801</a>

    <p>Broadgate Team discovered flaws in packet processing in the
    Broadcom wifi firmware and proprietary drivers that could lead to
    remote code execution.  However, this vulnerability is not
    believed to affect the drivers used in Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0561">CVE-2017-0561</a>

    <p>Gal Beniamini of Project Zero discovered a flaw in the TDLS
    implementation in Broadcom wifi firmware.  This could be exploited
    by an attacker on the same WPA2 network to execute code on the
    wifi microcontroller.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9417">CVE-2017-9417</a>

<p>/ #869639</p>

    <p>Nitay Artenstein of Exodus Intelligence discovered a flaw in the
    WMM implementation in Broadcom wifi firmware.  This could be
    exploited by a nearby attacker to execute code on the wifi
    microcontroller.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13077">CVE-2017-13077</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-13078">CVE-2017-13078</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-13079">CVE-2017-13079</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-13080">CVE-2017-13080</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2017-13081">CVE-2017-13081</a></p>

    <p>Mathy Vanhoef of the imec-DistriNet research group of KU Leuven
    discovered multiple vulnerabilities in the WPA protocol used for
    authentication in wireless networks, dubbed <q>KRACK</q>.</p>

    <p>An attacker exploiting the vulnerabilities could force the
    vulnerable system to reuse cryptographic session keys, enabling a
    range of cryptographic attacks against the ciphers used in WPA1
    and WPA2.</p>

    <p>These vulnerabilities are only being fixed for certain Broadcom
    wifi chips, and might still be present in firmware for other wifi
    hardware.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
20161130-4~deb8u1.  This version also adds new firmware and packages
for use with Linux 4.9, and re-adds firmware-{adi,ralink} as
transitional packages.</p>

<p>We recommend that you upgrade your firmware-nonfree packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1573.data"
# $Id: $
