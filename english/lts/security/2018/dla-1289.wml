<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there where a number of vulnerabilities in irssi,
the terminal based IRC client:</p>

  <p>- <a href="https://security-tracker.debian.org/tracker/CVE-2018-7050">CVE-2018-7050</a>: Null pointer dereference for an <q>empty</q> nick.</p>

  <p>- <a href="https://security-tracker.debian.org/tracker/CVE-2018-7051">CVE-2018-7051</a>: Certain nick names could result in out-of-bounds
    access when printing theme strings.</p>

  <p>- <a href="https://security-tracker.debian.org/tracker/CVE-2018-7052">CVE-2018-7052</a>: When the number of windows exceeds the available space, a
    crash could occur due to another NULL pointer dereference.</p>

<p>For Debian 7 <q>Wheezy</q>, these issues have been fixed in irssi version
0.8.15-5+deb7u5.</p>

<p>We recommend that you upgrade your irssi packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1289.data"
# $Id: $
