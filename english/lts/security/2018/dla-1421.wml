<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in the interpreter for the Ruby
language. The Common Vulnerabilities and Exposures project identifies the
following issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-9096">CVE-2015-9096</a>

    <p>SMTP command injection in Net::SMTP via CRLF sequences in a RCPT TO
    or MAIL FROM command.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2339">CVE-2016-2339</a>

    <p>Exploitable heap overflow in Fiddle::Function.new.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7798">CVE-2016-7798</a>

    <p>Incorrect handling of initialization vector in the GCM mode in the
    OpenSSL extension.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0898">CVE-2017-0898</a>

    <p>Buffer underrun vulnerability in Kernel.sprintf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0899">CVE-2017-0899</a>

    <p>ANSI escape sequence vulnerability in RubyGems.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0900">CVE-2017-0900</a>

    <p>DoS vulnerability in the RubyGems query command.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0901">CVE-2017-0901</a>

    <p>gem installer allowed a malicious gem to overwrite arbitrary files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0902">CVE-2017-0902</a>

    <p>RubyGems DNS request hijacking vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0903">CVE-2017-0903</a>

    <p>Max Justicz reported that RubyGems is prone to an unsafe object
    deserialization vulnerability. When parsed by an application which
    processes gems, a specially crafted YAML formatted gem specification
    can lead to remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10784">CVE-2017-10784</a>

    <p>Yusuke Endoh discovered an escape sequence injection vulnerability in
    the Basic authentication of WEBrick. An attacker can take advantage of
    this flaw to inject malicious escape sequences to the WEBrick log and
    potentially execute control characters on the victim's terminal
    emulator when reading logs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14033">CVE-2017-14033</a>

    <p>asac reported a buffer underrun vulnerability in the OpenSSL
    extension. A remote attacker could take advantage of this flaw to
    cause the Ruby interpreter to crash leading to a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14064">CVE-2017-14064</a>

    <p>Heap memory disclosure in the JSON library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17405">CVE-2017-17405</a>

    <p>A command injection vulnerability in Net::FTP might allow a
    malicious FTP server to execute arbitrary commands.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17742">CVE-2017-17742</a>

    <p>Aaron Patterson reported that WEBrick bundled with Ruby was vulnerable
    to an HTTP response splitting vulnerability. It was possible for an
    attacker to inject fake HTTP responses if a script accepted an
    external input and output it without modifications.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17790">CVE-2017-17790</a>

    <p>A command injection vulnerability in lib/resolv.rb's lazy_initialze
    might allow a command injection attack. However untrusted input to
    this function is rather unlikely.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6914">CVE-2018-6914</a>

    <p>ooooooo_q discovered a directory traversal vulnerability in the
    Dir.mktmpdir method in the tmpdir library. It made it possible for
    attackers to create arbitrary directories or files via a .. (dot dot)
    in the prefix argument.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8777">CVE-2018-8777</a>

    <p>Eric Wong reported an out-of-memory DoS vulnerability related to a
    large request in WEBrick bundled with Ruby.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8778">CVE-2018-8778</a>

    <p>aerodudrizzt found a buffer under-read vulnerability in the Ruby
    String#unpack method. If a big number was passed with the specifier @,
    the number was treated as a negative value, and an out-of-buffer read
    occurred. Attackers could read data on heaps if an script accepts an
    external input as the argument of String#unpack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8779">CVE-2018-8779</a>

    <p>ooooooo_q reported that the UNIXServer.open and UNIXSocket.open
    methods of the socket library bundled with Ruby did not check for NUL
    bytes in the path argument. The lack of check made the methods
    vulnerable to unintentional socket creation and unintentional socket
    access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8780">CVE-2018-8780</a>

    <p>ooooooo_q discovered an unintentional directory traversal in
    some methods in Dir, by the lack of checking for NUL bytes in their
    parameter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000075">CVE-2018-1000075</a>

    <p>A negative size vulnerability in ruby gem package tar header that could
    cause an infinite loop.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000076">CVE-2018-1000076</a>

    <p>RubyGems package improperly verifies cryptographic signatures. A mis-signed
    gem could be installed if the tarball contains multiple gem signatures.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000077">CVE-2018-1000077</a>

    <p>An improper input validation vulnerability in RubyGems specification
    homepage attribute could allow malicious gem to set an invalid homepage
    URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000078">CVE-2018-1000078</a>

    <p>Cross Site Scripting (XSS) vulnerability in gem server display of homepage
    attribute.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000079">CVE-2018-1000079</a>

    <p>Path Traversal vulnerability during gem installation.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.5-2+deb8u4.</p>

<p>We recommend that you upgrade your ruby2.1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1421.data"
# $Id: $
