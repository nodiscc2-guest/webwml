<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The security update of OpenSSH announced as DLA 1500-1 introduced a bug in
openssh-client: when X11 forwarding is enabled (via system-wide
configuration in ssh_config or via -X command line switch), but no DISPLAY
is set, the client produces a "DISPLAY "(null)" invalid; disabling X11
forwarding" warning. These bug was introduced by the patch set to fix the
<a href="https://security-tracker.debian.org/tracker/CVE-2016-1908">CVE-2016-1908</a> issue. For reference, the following is the relevant section
of the original announcement:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1908">CVE-2016-1908</a>

    <p>OpenSSH mishandled untrusted X11 forwarding when the X server disables
    the SECURITY extension. Untrusted connections could obtain trusted X11
    forwarding privileges. Reported by Thomas Hoger.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:6.7p1-5+deb8u7.</p>

<p>We recommend that you upgrade your openssh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>--H+4ONPRPur6+Ovig
Content-Type: application/pgp-signature; name="signature.asc"</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1500-2.data"
# $Id: $
