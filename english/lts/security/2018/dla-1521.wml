<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Fabien Arnoux discovered several security issues in email validation
of otrs system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16586">CVE-2018-16586</a>

    <p>Load external image or CSS resources in browser when user opens a
    malicious email.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16587">CVE-2018-16587</a>

    <p>Remote deletions of arbitrary files that the OTRS web server user
    has write access when opening malicious email.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.3.18-1+deb8u6.</p>

<p>We recommend that you upgrade your otrs2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1521.data"
# $Id: $
